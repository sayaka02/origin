package com.example;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class HelloWorldController{
//
	//マッピングの部分に/→この場合はURLの最後に/を足す。

	@RequestMapping("/HelloWorld")
	@ResponseBody
	public String home() {
		return "Hello World!!!";
    }
}
